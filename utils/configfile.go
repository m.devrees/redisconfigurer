package utils

import (
	"bufio"
	"os"
	"strings"
)

func Check(e error) {
	if e != nil {
		panic(e)
	}
}

func GetConfigFile() string {
	userhome, err := os.UserHomeDir()
	Check(err)
	path := userhome + "/.config/docker-setup.config"
	return path

}

func GetProjectsDir() string {
	path := GetConfigFile()
	f, err := os.Open(path)
	Check(err)
	defer f.Close()

	// Splits on newlines by default.
	scanner := bufio.NewScanner(f)

	line := 1
	// https://golang.org/pkg/bufio/#Scanner.Scan
	var installdir = ""
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), "installdir") {
			installdirVar := scanner.Text()
			parts := strings.Split(installdirVar, "=")
			//fmt.Println(parts[1])
			installdir = parts[1]
		}
		line++
	}
	projectsdir := installdir + "/data/shared/sites"

	if err := scanner.Err(); err != nil {
		// Handle the error
		Check(err)
	}
	return projectsdir
}
