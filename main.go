package main

import (
	"errors"
	"fmt"
	"os"
	"redisConfigurer/utils"
)

func main() {
	projectsdir := utils.GetProjectsDir()
	loopDir(projectsdir)
	//fmt.Println(projectsdir)
}

func loopDir(dir string) {
	items, err := os.ReadDir(dir)
	utils.Check(err)
	//fmt.Println(items)
	for _, item := range items {
		if item.IsDir() {
			if _, err := os.Stat(dir + "/" + item.Name() + "/app/etc/env.php"); errors.Is(err, os.ErrNotExist) {
				// path/to/whatever does not exist
				//fmt.Println(item.Name() + "not installed")
				continue
			}
			fmt.Println(item.Name())
		}
	}
}
